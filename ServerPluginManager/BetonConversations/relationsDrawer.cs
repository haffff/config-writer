﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServerPluginManager.BetonConversations
{
    public class RelationsDrawer
    {
        static public int relationIdCounter = 0;

        public List<KeyValuePair<int, KeyValuePair<PointF, PointF>>> relations = new List<KeyValuePair<int, KeyValuePair<PointF, PointF>>>();
        public void addRelation(int id,PointF a, PointF b)
        {
            relations.Add(new KeyValuePair<int,KeyValuePair<PointF, PointF>>(id, new KeyValuePair<PointF, PointF>(a, b)));
        }

        public void updateRelations(PictureBox px1)
        {
            px1.CreateGraphics().Clear(Color.DarkGray);
            foreach(KeyValuePair<int,KeyValuePair<PointF, PointF>> a in relations)
            {
                px1.CreateGraphics().DrawLine(new Pen(Color.Black, 5), a.Value.Key,a.Value.Value);
            }
        }


        public void changeRelation(int idRelation, PointF newA, PointF newB)
        {
            deleteRelation(idRelation);
            addRelation(idRelation, newA, newB);
        }

        public void deleteRelation(int idRelation)
        {
            relations.Remove(relations.FirstOrDefault(o => o.Key == idRelation));
        }


    }
}
