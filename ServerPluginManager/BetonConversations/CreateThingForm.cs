﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServerPluginManager
{
    public partial class CreateThingForm : Form
    {
        public CreateThingForm(string type ,BetonConversations.FileManip fileM,BetonConversationForm parent)
        {
            InitializeComponent();
            if(type == "event")
            {
                resources = CreateForm.Resources.eventsComponentsList;
                source = CreateForm.Resources.eventsSource;
            }

            if (type == "condition")
            {
                resources = CreateForm.Resources.conditionsComponentsList;
                source = CreateForm.Resources.conditionsSource;
            }
            this.type = type;
            manip = fileM;

            eventsListbox.Items.Clear();
            eventsListbox.Items.AddRange(resources.Keys.ToArray());
            //linkLabel1.Add(source);
            this.parent = parent;
            
        }
        BetonConversationForm parent;
        Dictionary<string, string> resources;
        string source;
        string type;
        BetonConversations.FileManip manip;
        private void eventsListbox_Click(object sender, EventArgs e)
        {
            resources.TryGetValue(eventsListbox.SelectedItem.ToString(),out string a);
            if(a!= null)
            label1.Text = a;
        }

        private void eventsListbox_DoubleClick(object sender, EventArgs e)
        {
            resources.TryGetValue(eventsListbox.SelectedItem.ToString(), out string a);
            if (a != null)
                textBox1.Text = a;
        }

        private void button1_Click(object sender, EventArgs e)
        {
                manip.enterValue(nameBox.Text.Replace(" ","_") + ": '" + textBox1.Text +"'", type + "s.yml");
                    parent.updateEventsAndConditions();
                    this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Are you sure?","",MessageBoxButtons.OKCancel)==DialogResult.Cancel)
            {
                return;
            }
            this.Close();
        }

        private void comboBox1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            listBox1.Items.Clear();

            switch(comboBox1.SelectedItem.ToString())
            {
                case "Events":
                    listBox1.Items.AddRange(manip.GetEvents().Keys.ToArray());
                break;
                case "Conditions":
                    listBox1.Items.AddRange(manip.GetConditions().Keys.ToArray());
                break;
                case "Items":
                listBox1.Items.AddRange(manip.GetNamesFromFiles("items.yml").ToArray());
                break;
                case "Objectives":
                 listBox1.Items.AddRange(manip.GetNamesFromFiles("objectives.yml").ToArray());
                break;
            }

        }

        private void listBox1_Click(object sender, EventArgs e)
        {
            if(listBox1.SelectedItem == null)
            {
                return;
            }
            string a = listBox1.SelectedItem.ToString();
            if (a != null)
            textBox1.Text += a;
        }
    }
}
