﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerPluginManager.BetonConversations
{
    public enum ToolsAction
    {
        Move,
        Delete,
        AssingEvent,
        AssingCondition,
        Relation,
        DeleteRelation
    }
}
