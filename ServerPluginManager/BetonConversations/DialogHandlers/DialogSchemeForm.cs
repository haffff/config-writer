﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace ServerPluginManager.BetonConversations
{
    class DialogSchemeForm
    {
       public GroupBox group { get; set; }

        public Label NameLabel { get; set; }
        public TextBox Name { get; set; }

        public Label TextLabel { get; set; }
        public TextBox Text { get; set; }

        public Label Events { get; set; }
        public Label Conditions { get; set; }

        public CheckBox IsPlayer { get; set; }

        public List<string> Pointers { get; set; }

        public PointF PointerLower { get; }
        public PointF PointerUpper { get; }

        public Dictionary<int, bool> idRelations;

        public DialogSchemeForm()
        {
            idRelations = new Dictionary<int, bool>();
            Pointers = new List<string>();
            group = new GroupBox();

            NameLabel = new Label();
            TextLabel = new Label();
            Name = new TextBox();
            Text = new TextBox();
            IsPlayer = new CheckBox();
            NameLabel.AutoSize = true;
            NameLabel.Location = new System.Drawing.Point(10, 10);
            NameLabel.BackColor = System.Drawing.Color.Transparent;
            NameLabel.Text = "Name:";
            Name.Location = new System.Drawing.Point(50, 10);

            Name.Multiline = true;
            Name.MinimumSize = new System.Drawing.Size(75, 24);
            Name.Size = new System.Drawing.Size(150, 24);
            Name.Multiline = false;


            TextLabel.AutoSize = true;
            TextLabel.Location = new System.Drawing.Point(10, 40);
            TextLabel.Text = "Text:";
            Text.Location = new System.Drawing.Point(50, 40);

            Text.Multiline = true;
            Text.MinimumSize = new System.Drawing.Size(75, 24);
            Text.Size = new System.Drawing.Size(150, 24);
            Text.Multiline = false;

            IsPlayer.Location = new System.Drawing.Point(10, 70);
            IsPlayer.Text = "Is this Player dialog?";
            IsPlayer.AutoSize = true;


            group.BackColor = System.Drawing.Color.Aquamarine;
            group.Controls.Add(NameLabel);
            group.Controls.Add(Name);
            group.Controls.Add(TextLabel);
            group.Controls.Add(Text);
            group.Controls.Add(IsPlayer);

            group.AutoSize = true;
            group.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            group.Location = new System.Drawing.Point(20, 20);

            PointerUpper = new PointF(62,1);
            PointerLower = new PointF(62,75);
            Events = new Label();
            Events.AutoSize = true;
            Events.Location = new System.Drawing.Point(150 , 70);
            Events.BackColor = System.Drawing.Color.Transparent;
            Events.Text = "Events: ";

            Conditions = new Label();
            Conditions.AutoSize = true;
            Conditions.Location = new System.Drawing.Point(150, 85);
            Conditions.BackColor = System.Drawing.Color.Transparent;
            Conditions.Text = "Conditions: ";
            group.Controls.Add(Events);
            group.Controls.Add(Conditions);

            group.Show();
        }
    }


}
