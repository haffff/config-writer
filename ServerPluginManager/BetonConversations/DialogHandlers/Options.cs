﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerPluginManager.BetonConversations
{
    public class Options
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public List<string> Pointers { get; set; }
        public List<string> Conditions { get; set; }
        public List<string> Events { get; set; }
    }
}
