﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ServerPluginManager.BetonConversations
{
    public class DialogScheme
    {
        public DialogScheme()
        {
            NpcOptions = new List<Options>();
            PlayerOptions = new List<Options>();
        }


        public string NpcName { get; set; }
        public int NpcId { get; set; }
        public bool Stop { get; set; }
        public string First { get; set; }
        public List<Options> NpcOptions { get; set; }
        public List<Options> PlayerOptions { get; set; }
    }
}
