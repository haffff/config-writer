﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerPluginManager.BetonConversations
{
    static class BetonConverter
    {

        static List<string> getYamlOptions()
        {
            List<string> option = new List<string>();
            option.Add("  '<NAME>': ");
            option.Add("    text: '<TEXT>'");
            option.Add("    conditions: '<CONDITIONS>'");
            option.Add("    events: '<EVENTS>'");
            option.Add("    pointers: '<POINTERS>'");
            return option;
        }

        static List<string> getYamlMain()
        {
            List<string> option = new List<string>();
            option.Add("quester: '<NPC_NAME>'");
            option.Add("first: '<F>'");
            option.Add("stop: '<STOP>'");
         //   option.Add("final_events: '<FEVENTS>'");
            option.Add("NPC_options: ");
            option.Add("<NPC_OPTIONS>");
            option.Add("player_options:");
            option.Add("<PLAYER_OPTIONS>");
            return option;
        }





        static public List<string> toYaml(DialogScheme dialog)
        {
            List<string> final = new List<string>();

            List<string> ymlMain = getYamlMain();

            List<string> ymlOP = getYamlOptions();

            foreach (string a in ymlMain)
            {

                if (a == "<NPC_OPTIONS>")
                {
                    foreach (Options b in dialog.NpcOptions)
                    {
                        foreach (string c in ymlOP)
                        {
                            string d = c;
                            d = d.Replace("<NAME>", b.Name);
                            d = d.Replace("<LANG>", "pl");                                  //TODO
                            d = d.Replace("<TEXT>", b.Text);

                            d = d.Replace("<POINTERS>", String.Join(",", b.Pointers.ToArray()));

                            d = d.Replace("<EVENTS>", String.Join(",", b.Events.ToArray()));

                            d = d.Replace("<CONDITIONS>", String.Join(",", b.Conditions.ToArray()));

                            d = d.Replace(",'","'");

                            if (!d.Contains("<CONDITIONS>") && !d.Contains("<EVENTS>") && !d.Contains("<POINTERS>"))
                                final.Add(d);

                        }
                    }
                    continue;
                }

                if (a == "<PLAYER_OPTIONS>")
                {
                    foreach (Options b in dialog.PlayerOptions)
                    {
                        foreach (string c in ymlOP)
                        {
                            string d = c;
                            d = d.Replace("<NAME>", b.Name);
                            d = d.Replace("<LANG>", "pl");                                  //TODO
                            d = d.Replace("<TEXT>", b.Text);

                            d = d.Replace("<POINTERS>", String.Join(",", b.Pointers.ToArray()));

                            d = d.Replace("<EVENTS>", String.Join(",", b.Events.ToArray()));

                            d = d.Replace("<CONDITIONS>", String.Join(",", b.Conditions.ToArray()));
                            d = d.Replace(",'", "'");
                            if (!d.Contains("<CONDITIONS>") && !d.Contains("<EVENTS>")&&!d.Contains("<POINTERS>"))
                                final.Add(d);

                        }
                    }
                    continue;
                }

                string ed = a.Replace("<LANG>", dialog.First);
                ed = ed.Replace("<STOP>", dialog.Stop.ToString());
                ed = ed.Replace("<F>", dialog.First);
                ed = ed.Replace("<NPC_NAME>", dialog.NpcName);


                final.Add(ed);

            }

            
            return final;
        }


        static public Options toDialogScheme(DialogSchemeForm form)
        {
            Options final = new Options();
            final.Name = form.Name.Text;
            final.Text = form.Text.Text;
            final.Pointers = form.Pointers;

            string eventsString = form.Events.Text;
            eventsString = eventsString.Replace("Events: ","");

            final.Events = eventsString.Split(' ').ToList();

             eventsString = form.Conditions.Text;
            eventsString = eventsString.Replace("Conditions: ", "");

            final.Conditions = eventsString.Split(' ').ToList();

            return final;
        }
    }
}
