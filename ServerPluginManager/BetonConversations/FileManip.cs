﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace ServerPluginManager.BetonConversations
{
    public class FileManip
    {
        string workingDir;

        public string setWorkingDir()
        {


            FolderBrowserDialog of = new FolderBrowserDialog();

            of.Description = "Chose path to work with. it should be location with main.yml";
            if(of.ShowDialog()== DialogResult.OK)
            {
                workingDir = of.SelectedPath;
                return workingDir;
            }

            return null;
        }

        public string dialogSave(string Name)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            //saveFileDialog.AddExtension = true;
            saveFileDialog.Filter = ".yml|Yaml config files";
            saveFileDialog.Title = "Conversation save place WARNING: IT MUST BE THIS \"conversation\" IN QUEST IF YOU WANT QUEST TO WORK";
            saveFileDialog.InitialDirectory = workingDir + "\\conversations";
            saveFileDialog.FileName = Name;
            saveFileDialog.DefaultExt = ".yml";

            if (saveFileDialog.ShowDialog() != DialogResult.OK)
                return null;
            return saveFileDialog.FileName;
        }

        public void enterNpcID(int id,string npcname)
        {
            if(!File.Exists(workingDir + "\\main.yml"))
            return;

            List<string> yml = File.ReadAllLines(workingDir + "\\main.yml").ToList();
            if (yml.FirstOrDefault(o => o.Contains(npcname)) != null)
            {
                if (MessageBox.Show("That Npc already exists, Overwrite?", "", MessageBoxButtons.YesNo) == DialogResult.No)
                    return;
                        int index = yml.IndexOf(yml.FirstOrDefault(o => o.Contains(npcname)));
                        yml.Remove(yml.FirstOrDefault(o => o.Contains(npcname)));
                        yml.Insert(index, "  '" + id + "' : " + npcname);
                return;
            }
            for (int a = 0;a < yml.Count; a++)
            {
                if(yml[a] == "npcs:")
                {
                    yml.Insert(a + 1, "  '"+id+"' : "+npcname);
                }
            }

            File.WriteAllLines(workingDir + "\\main.yml", yml.ToArray());

            return;
        }



        public void saveFinalYaml(string path, List<string> yamlFile)
        {
            if(File.Exists(path))
            {
                File.Delete(path);
            }
            File.WriteAllLines(path, yamlFile.ToArray());
        }



        public Dictionary<string, string> GetEvents()
        {
            return GetDataFromFiles(workingDir + "\\events.yml");
        }

        public Dictionary<string, string> GetConditions()
        {
            checkMain();
            return GetDataFromFiles(workingDir + "\\conditions.yml");
        }

        public void checkMain()
        {
            if (!File.Exists(workingDir + "\\main.yml"))
            {
                errorFileNotFound(workingDir + "\\main.yml");
            }
        }

        void errorFileNotFound(string file)
        {
            MessageBox.Show("One of the files needed to the work does not exists. Are you sure this is proper path? \n Missing file: " + file);
        }


        public List<string> GetNamesFromFiles(string file)
        {
            List<string> final = new List<string>();
            if (!File.Exists(workingDir + "\\" + file))
            {

                errorFileNotFound(file);
                return final;
            }

            final = File.ReadAllLines(workingDir + "\\" + file).ToList();
            for (int i = 0; i < final.Count; i++)
            {
                int a = final[i].IndexOf(':');
                if (a >= 0)
                {
                    final[i] = final[i].Remove(a);
                }
                if(String.IsNullOrEmpty(final[i]))
                {
                    final.Remove(final[i]);
                }
            }
            return final;
        }

        // Nazwa  Polecenie
        Dictionary<string,string> GetDataFromFiles(string file)
        {
            Dictionary<string, string> final = new Dictionary<string, string>();
            if (!File.Exists(file))
            {
                
                errorFileNotFound(file);
                return final;
            }
            List<string> tmpLines = File.ReadAllLines(file).ToList();
            for (int i = 0; i < tmpLines.Count;i++)
            {
                int a2 = tmpLines[i].LastIndexOf('\'');
                int a1 = tmpLines[i].IndexOf('\'');
                if(a1<a2)
                {
                    string s1 = tmpLines[i].Substring(a1, a2 - a1);
                    string s2 = tmpLines[i].Remove(tmpLines[i].IndexOf(':'));
                    final.Add(s2, s1);
                }
                else
                {
                    string s2 = tmpLines[i].Remove(tmpLines[i].IndexOf(':'));
                    string s1 = tmpLines[i].Substring(a1);
                    i++;
                    while (!tmpLines.Contains("\'"))
                    {
                        s1 += tmpLines[i];
                        i++;
                    }
                    s1 += tmpLines[i];
                    final.Add(s2, s1);
                }
            }
            return final;
        }

        public void enterValue(string Value, string path)
        {
            if (!File.Exists(workingDir + "\\" + path))
                return;

            List<string> yml = File.ReadAllLines(workingDir + "\\" + path).ToList();
            yml.Add(Value);

            File.WriteAllLines(workingDir + "\\" + path,yml.ToArray());
            return;
        }


    }
}
