﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServerPluginManager
{
    public partial class BetonConversationForm : Form
    {
        public BetonConversationForm(MainForm form1)
        {
            if (String.IsNullOrEmpty(fileManip.setWorkingDir()))
            {
                this.Close();
                return;
            }


            InitializeComponent();
            
            Events = fileManip.GetEvents();
            Conditions = fileManip.GetConditions();

            
            dialogMoveTimer.Interval = 10;
            dialogMoveTimer.Enabled = false;
            dialogMoveTimer.Tick += EventMoveDialog;
            back = pictureBox1.CreateGraphics();

            EventsBox.Items.AddRange(Events.Keys.ToArray());
            ConditionsBox.Items.AddRange(Conditions.Keys.ToArray());
            Parent = form1;

            this.Show();
        }
        MainForm Parent;

        BetonConversations.FileManip fileManip = new BetonConversations.FileManip();

        BetonConversations.RelationsDrawer relations = new BetonConversations.RelationsDrawer();
        
        Timer dialogMoveTimer = new Timer();
        List<BetonConversations.DialogSchemeForm> DialogList = new List<BetonConversations.DialogSchemeForm>();

        Dictionary<string, string> Events;
        Dictionary<string, string> Conditions;


        BetonConversations.ToolsAction action = BetonConversations.ToolsAction.Move;
        GroupBox currWin;
        GroupBox lastCurrWin;
        Graphics back;

        void EventMoveDialog(object sender,EventArgs e)
        {
            
            currWin.Location = new Point(Cursor.Position.X - this.DesktopLocation.X - 15, Cursor.Position.Y - this.DesktopLocation.Y - 40);
        }

        

        public void OnDown(object sender, MouseEventArgs e)
        {
            currWin = (GroupBox)sender;

            switch (action)
            {
                case BetonConversations.ToolsAction.Move:
                    dialogMoveTimer.Start();
                    break;
                case BetonConversations.ToolsAction.Delete:
                    
                    foreach (KeyValuePair<int, bool> a in DialogList.FirstOrDefault(o => o.group == currWin).idRelations)
                    {
                        DialogList.FirstOrDefault(o => o.idRelations.Keys.Contains(a.Key) && o.group != currWin).idRelations.Remove(a.Key);
                        
                        relations.deleteRelation(a.Key);
                        relations.updateRelations(pictureBox1);
                    }

                    DialogList.Remove(DialogList.First(o => o.group == currWin));
                    Controls.Remove(currWin);
                    break;
                case BetonConversations.ToolsAction.Relation:
                     if (lastCurrWin == null || lastCurrWin == currWin)
                    {
                         lastCurrWin = currWin;
                        lastCurrWin.BackColor = Color.Orange;
                    }
                    else
                    {
                        makeRelation();
                        lastCurrWin.BackColor = Color.Aquamarine;
                        lastCurrWin = null;
                    }
                break;
                case BetonConversations.ToolsAction.DeleteRelation:
                    if (lastCurrWin == null || lastCurrWin == currWin)
                    {
                        lastCurrWin = currWin;
                        lastCurrWin.BackColor = Color.Red;
                    }
                    else
                    {
                        deleteRelation();
                        lastCurrWin.BackColor = Color.Aquamarine;
                        lastCurrWin = null;
                    }
                    break;
            }

        }

        public void OnUp(object sender, MouseEventArgs e)
        {
            relations.updateRelations(pictureBox1);
            dialogMoveTimer.Stop();
            dialogMoveTimer.Dispose();
            

            if (action == BetonConversations.ToolsAction.Move && DialogList.FirstOrDefault(o => o.group == currWin).idRelations != null && DialogList.FirstOrDefault(o => o.group == currWin).idRelations.Count > 0)
                foreach (KeyValuePair<int, bool> a in DialogList.FirstOrDefault(o => o.group == currWin).idRelations)
                {
                    if (a.Value)
                    {
                        PointF b = DialogList.FirstOrDefault(o => o.idRelations.Contains(new KeyValuePair<int, bool>(a.Key, !a.Value))).PointerLower;
                        PointF b1 = DialogList.FirstOrDefault(o => o.idRelations.Contains(new KeyValuePair<int, bool>(a.Key, !a.Value))).group.Location;
                        relations.changeRelation(a.Key, Utility.AddPoints(b, b1), Utility.AddPoints(DialogList.FirstOrDefault(o => o.group == currWin).PointerUpper , currWin.Location));
                    }
                    else
                    {
                        PointF b = DialogList.FirstOrDefault(o => o.idRelations.Contains(new KeyValuePair<int, bool>(a.Key, !a.Value))).PointerUpper;
                        PointF b1 = DialogList.FirstOrDefault(o => o.idRelations.Contains(new KeyValuePair<int, bool>(a.Key, !a.Value))).group.Location;
                        
                        relations.changeRelation(a.Key,Utility.AddPoints(DialogList.FirstOrDefault(o => o.group == currWin).PointerLower,currWin.Location), Utility.AddPoints(b, b1));
                    }

                    relations.updateRelations(pictureBox1);
                }
        }

        private void moveButton_Click(object sender, EventArgs e)
        {
            deleteInfoAboutRelations();
            action = BetonConversations.ToolsAction.Move;
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            deleteInfoAboutRelations();
            action = BetonConversations.ToolsAction.Delete;
        }

        private void relateButton_Click(object sender, EventArgs e)
        {
            deleteInfoAboutRelations();
            action = BetonConversations.ToolsAction.Relation;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            deleteInfoAboutRelations();
            action = BetonConversations.ToolsAction.DeleteRelation;
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This funcionality is not implemented yet.");
        }
    }
}
