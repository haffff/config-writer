﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServerPluginManager
{
    public partial class BetonConversationForm : Form
    {
        private void dialogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BetonConversations.DialogSchemeForm tmpDialogScheme = new BetonConversations.DialogSchemeForm();
            tmpDialogScheme.group.MouseDown += new MouseEventHandler(OnDown);
            tmpDialogScheme.group.MouseUp += new MouseEventHandler(OnUp);
            DialogList.Add(tmpDialogScheme);

            Controls.Add(tmpDialogScheme.group);

            pictureBox1.SendToBack();
        }



        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string path = fileManip.dialogSave(NpcText.Text);
            if (String.IsNullOrEmpty(path))
            {
                MessageBox.Show("File save cancelled");
                return;
            }

            BetonConversations.DialogScheme scheme = new BetonConversations.DialogScheme();

            foreach (BetonConversations.DialogSchemeForm a in DialogList)
            {
                foreach (KeyValuePair<int, bool> pair in a.idRelations)
                {
                    if (!pair.Value)
                    {
                        a.Pointers.Add(DialogList.FirstOrDefault(o => o.idRelations.Contains(new KeyValuePair<int, bool>(pair.Key, true))).Name.Text);
                    }
                }

                if (a.IsPlayer.Checked)
                {
                    scheme.PlayerOptions.Add(BetonConversations.BetonConverter.toDialogScheme(a));
                }
                else
                {
                    scheme.NpcOptions.Add(BetonConversations.BetonConverter.toDialogScheme(a));
                }

                scheme.First = FirstDialog.Text;
                scheme.Stop = StopBox.Checked;
                scheme.NpcName = NpcText.Text;
                Int32.TryParse(npcIDNumeric.Text, out int b);
                scheme.NpcId = b;


                fileManip.saveFinalYaml(path, BetonConversations.BetonConverter.toYaml(scheme));

            }
            fileManip.enterNpcID((int)npcIDNumeric.Value, NpcText.Text);
            MessageBox.Show("File Saved");
        }


        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to leave without save?", "", MessageBoxButtons.YesNo) == DialogResult.No)
            { return; }
            this.Close();
        }

        private void conditionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateThingForm a = new CreateThingForm("condition", fileManip, this);
            a.Show();


            Conditions = fileManip.GetConditions();
            ConditionsBox.Items.Clear();
            ConditionsBox.Items.AddRange(Conditions.Keys.ToArray());
        }

        private void eventToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateThingForm a = new CreateThingForm("event", fileManip, this);
            a.Show();
        }

        private void newToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var x = (Button)(Parent.Controls.Find("betonConversationsFormButton",false)[0]);
             x.PerformClick();
        }

        private void tagToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Microsoft.VisualBasic.Interaction.InputBox("Write tag name", "Tag Creation", "Default Text");
        }
    }
}
