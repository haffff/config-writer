﻿using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServerPluginManager
{
    public partial class BetonConversationForm : Form
    {

        void deleteInfoAboutRelations()
        {
            if (lastCurrWin != null)
                lastCurrWin.BackColor = Color.Aquamarine;
            lastCurrWin = null;
        }

        void makeRelation()
        {
            BetonConversations.RelationsDrawer.relationIdCounter++;
            PointF pt1 = DialogList.FirstOrDefault(o => o.group == lastCurrWin).PointerLower;
            PointF pt2 = DialogList.FirstOrDefault(o => o.group == currWin).PointerUpper;

            PointF pt1Relative = Utility.AddPoints(lastCurrWin.Location, pt1);
            PointF pt2Relative = Utility.AddPoints(currWin.Location, pt2);

            relations.addRelation(BetonConversations.RelationsDrawer.relationIdCounter, pt1Relative, pt2Relative);

            DialogList.FirstOrDefault(o => o.group == lastCurrWin).idRelations.Add(BetonConversations.RelationsDrawer.relationIdCounter, false);
            DialogList.FirstOrDefault(o => o.group == currWin).idRelations.Add(BetonConversations.RelationsDrawer.relationIdCounter, true);

            //DialogList.FirstOrDefault(o => o.group == lastCurrWin).Pointers.Add(DialogList.FirstOrDefault(o => o.group == currWin).Name.Text);

            relations.updateRelations(pictureBox1);
        }

        void deleteRelation()
        {
            BetonConversations.DialogSchemeForm a = DialogList.FirstOrDefault(o => o.group == currWin);
            BetonConversations.DialogSchemeForm b = DialogList.FirstOrDefault(o => o.group == lastCurrWin);
            int key = -1;
            foreach (int k in a.idRelations.Keys)
            {
                KeyValuePair<int, bool> tmp = b.idRelations.FirstOrDefault(o => o.Key == k);
                if (tmp.Key != 0)
                {
                    key = k;
                    break;
                }
            }
            if (key > -1)
            {
                relations.deleteRelation(key);
                a.idRelations.Remove(key);
                b.idRelations.Remove(key);
                relations.updateRelations(pictureBox1);
            }
            else
            { return; }

        }
    }
}
