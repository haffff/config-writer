﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServerPluginManager
{
    public partial class BetonConversationForm : Form
    {

        public void updateEventsAndConditions()
        {
            Conditions = fileManip.GetConditions();
            ConditionsBox.Items.Clear();
            ConditionsBox.Items.AddRange(Conditions.Keys.ToArray());
            Events = fileManip.GetEvents();
            EventsBox.Items.Clear();
            EventsBox.Items.AddRange(Events.Keys.ToArray());
        }

        private void EventsBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (currWin == null || EventsBox.Items == null || EventsBox.Items.Count == 0)
            {
                return;
            }
            BetonConversations.DialogSchemeForm a = DialogList.FirstOrDefault(o => o.group == currWin);

            if (a.Events.Text.Contains(EventsBox.SelectedItem.ToString()))
            {
                a.Events.Text = a.Events.Text.Replace(EventsBox.SelectedItem.ToString() + " ", "");
            }
            else
                a.Events.Text += EventsBox.SelectedItem.ToString() + " ";
        }

        private void ConditionsBox_DoubleClick(object sender, EventArgs e)
        {
            if (currWin == null || ConditionsBox.Items == null || ConditionsBox.Items.Count == 0)
            {
                return;
            }
            BetonConversations.DialogSchemeForm a = DialogList.FirstOrDefault(o => o.group == currWin);

            string condition = ConditionsBox.SelectedItem.ToString();
            if (checkBox1.Checked)
            {
                condition = "!" + condition;
            }

            if (a.Conditions.Text.Contains(ConditionsBox.SelectedItem.ToString()))
            {
                a.Conditions.Text = a.Conditions.Text.Replace("!" + condition + " ", "");
                a.Conditions.Text = a.Conditions.Text.Replace(condition + " ", "");
            }
            else
                a.Conditions.Text += condition + " ";
        }

        private void EventsBox_Click(object sender, EventArgs e)
        {
            if (EventsBox.Items == null || EventsBox.Items.Count == 0 || EventsBox.SelectedItem == null)
            { return; }
            string s;
            if (Events.TryGetValue(EventsBox.SelectedItem.ToString(), out s))
                label3.Text = s.Remove(0,1);
            else
                return;
        }

        private void ConditionsBox_Click(object sender, EventArgs e)
        {
            if (ConditionsBox.Items == null || ConditionsBox.Items.Count == 0 || ConditionsBox.SelectedItem == null)
            { return; }
            string s;
            if (Conditions.TryGetValue(ConditionsBox.SelectedItem.ToString(), out s))
                label3.Text = s.Remove(0, 1);
            else
                return;
        }
    }
}
