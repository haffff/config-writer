﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerPluginManager.CreateForm
{
    static public class Resources
    {
        public static Dictionary<string,string> eventsComponentsList;
        public static Dictionary<string, string> variablesComponentsList;
        public static Dictionary<string, string> conditionsComponentsList;
        public static string eventsSource = "https://github.com/Co0sh/BetonQuest/wiki/Events-List";
        public static string conditionsSource = "https://github.com/Co0sh/BetonQuest/wiki/Conditions-List";
        static Resources()
        {
            eventsComponentsList = new Dictionary<string, string>();

            //EVENTS

            eventsComponentsList.Add("Message", "message ({lang} &{mccolor}) {Message}");
            eventsComponentsList.Add("Command", "command {command}(|{command...})");
            eventsComponentsList.Add("Teleport", "teleport {x};{y};{z};{world}(;{headyaw};{headpitch})");
            eventsComponentsList.Add("Point", "point {category} ({*/-}){amount}");
            eventsComponentsList.Add("Tag", "tag {add/del} {name_of_the_tag}");
            eventsComponentsList.Add("Objective", "objective {start/delete/finish} {name_of_the_objective}");
            eventsComponentsList.Add("Journal", "journal {add/del/update} {entry only when add/del}");
            eventsComponentsList.Add("Lightning", "lightning {x};{y};{z};{world}");
            eventsComponentsList.Add("Explosion", "explosion {0/1 fire spread} {0/1 destroy blocks} {number_power} {x};{y};{z};{world}");
            eventsComponentsList.Add("Give", "give {item}:{Amount}(,{item}:{amount...}");
            eventsComponentsList.Add("Take from player", "take {item}:{amount}(,{item}:{amount...})");
            eventsComponentsList.Add("Potion Effect", "effect {effectType} {how_long} {level} (--ambient)");
            eventsComponentsList.Add("Conversation", "conversation {id}");
            eventsComponentsList.Add("Kill Player", "kill");
            eventsComponentsList.Add("Spawn a mob", "spawn {x};{y};{z};{world} {Type} (name:{name} marked:{objective} h:{helmet} c:{chestplate} l:{} b:{} drops:{item}{amount},{item}{amount}...)");
            eventsComponentsList.Add("Time", "time (+/-){number}");
            eventsComponentsList.Add("Weather", "weather {sun/storm/rain}");
            eventsComponentsList.Add("Folder(event pack)", "folder {event},{event}(,{event}...)");
            eventsComponentsList.Add("Set Block", "setblock {blockType} {x};{y};{z};{world}");
            eventsComponentsList.Add("Damage player", "damage {amount(1=half heart)}");
            eventsComponentsList.Add("Party event", "party {radius} {condition}(,{condition}...) {event}(,{event}...) ");
            eventsComponentsList.Add("Clear Mobs", "clear {mobtype}(,{}...) {x};{y};{z};{world} {radius} (name:{name} marked:{objective}) ");
            eventsComponentsList.Add("Run multiple instructions", "run ^{event/condition}(,^{}...)  ");
            eventsComponentsList.Add("Give Journal", "givejournal");
            eventsComponentsList.Add("Command as player", "sudo {command}(|{command...})");
            eventsComponentsList.Add("Take from chest", "chesttake {x};{y};{z};{world} {item}:{Amount}(,{i}:{a...} ");
            eventsComponentsList.Add("Take everything from chest", "chestclear {x};{y};{z};{world}");
            eventsComponentsList.Add("Compass", "compass {add/del} {Compass_Location_name}");
            eventsComponentsList.Add("Cancel", "cancel {quest_canceler}");
            eventsComponentsList.Add("Score", "score {scoreboard} {(+-*/)points}");
            eventsComponentsList.Add("Switch Lever", "lever {x};{y};{z};{world} {on/off/toggle}");
            eventsComponentsList.Add("Control Door", "door {x};{y};{z};{world} {on/off/toggle}");
            eventsComponentsList.Add("If Event else Event", "if {condition} {event} else {event}");
            eventsComponentsList.Add("Variable", "variable {variableid} {key} {value}");

            conditionsComponentsList = new Dictionary<string, string>();

            conditionsComponentsList.Add("item", "item {item}:{Amount}(,{item}:{amount...}");
            conditionsComponentsList.Add("hand", "hand {item}");
            conditionsComponentsList.Add("or", "or {condition},{condition}(,{condition}...)");
            conditionsComponentsList.Add("and", "and {condition},{condition}(,{condition}...)");
            conditionsComponentsList.Add("location", "location {x};{y};{z};{world} {radius}");
            conditionsComponentsList.Add("Health (more or equal)", "health {amount}");
            conditionsComponentsList.Add("(minecraft)Permission", "permission {Permission}");
            conditionsComponentsList.Add("Points (more or equal)", "points {category} {amount} (equal)");
            conditionsComponentsList.Add("Has tag", "tag {tag}");
            conditionsComponentsList.Add("Has Armor(definied in items.yml)", "armor {name}");
            conditionsComponentsList.Add("Potion Effect", "effect {effect}");
            conditionsComponentsList.Add("Time", "time {start}-{end}");
            conditionsComponentsList.Add("Weather", "weather {weather}");
            conditionsComponentsList.Add("Height", "height {height}");
            conditionsComponentsList.Add("Armor Rating", "rating {number}");
            conditionsComponentsList.Add("Random", "random {true times out}-{of number}");
            conditionsComponentsList.Add("Sneak", "sneak");
            conditionsComponentsList.Add("Journal entry", "journal {entry}");
            conditionsComponentsList.Add("Test for block (is this block there?)", "testforblock {x};{y};{z};{world} {blocktype}");
            conditionsComponentsList.Add("Empty slots in inventory", "empty {amount}");
            conditionsComponentsList.Add("party", "party {radius} (every:{conditions} any:{conditions} some:{conditions})");
            conditionsComponentsList.Add("Monsters in area", "monsters {mobtype}:{amount}(,{}:{}...) {x};{y};{z};{world} {radius} (name:{name})");
            conditionsComponentsList.Add("Objective", "objective {Objective}");
            conditionsComponentsList.Add("Check multiple instructions", "check ^{condition}(,^{}...)  ");
            conditionsComponentsList.Add("Check items in chest", "chestitem {x};{y};{z};{world} {item}:{Amount}(,{i}:{a...} ");
            conditionsComponentsList.Add("Score", "score {scoreboard} {points}");
            conditionsComponentsList.Add("World", "world {world}");
            conditionsComponentsList.Add("Variable", "variable {see in conditionslist}");
        }
    }
}
