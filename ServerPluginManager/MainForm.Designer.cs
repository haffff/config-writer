﻿namespace ServerPluginManager
{
    partial class MainForm
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.betonConversationsFormButton = new System.Windows.Forms.Button();
            this.objButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // betonConversationsFormButton
            // 
            this.betonConversationsFormButton.Location = new System.Drawing.Point(12, 12);
            this.betonConversationsFormButton.Name = "betonConversationsFormButton";
            this.betonConversationsFormButton.Size = new System.Drawing.Size(260, 36);
            this.betonConversationsFormButton.TabIndex = 0;
            this.betonConversationsFormButton.Text = "Beton Conservations";
            this.betonConversationsFormButton.UseVisualStyleBackColor = true;
            this.betonConversationsFormButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // objButton
            // 
            this.objButton.Location = new System.Drawing.Point(12, 54);
            this.objButton.Name = "objButton";
            this.objButton.Size = new System.Drawing.Size(260, 42);
            this.objButton.TabIndex = 1;
            this.objButton.Text = "Beton Objectives";
            this.objButton.UseVisualStyleBackColor = true;
            this.objButton.Click += new System.EventHandler(this.objButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 160);
            this.Controls.Add(this.objButton);
            this.Controls.Add(this.betonConversationsFormButton);
            this.Name = "MainForm";
            this.Text = "Sever plugins menager";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button betonConversationsFormButton;
        private System.Windows.Forms.Button objButton;
    }
}

